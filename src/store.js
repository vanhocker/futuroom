import Vue from "vue";
import Vuex from "vuex";
import * as Api from "./api";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    votes: [],
    pagination: {}
  },
  mutations: {
    setVotes(state, data) {
      state.pagination = data.pagination;
      state.votes.push(...data.votes);
    }
  },
  actions: {
    getVotes({ commit }, payload) {
      return new Promise(resolve => {
        Api.getVotes(payload, data => {
          if (data && typeof data === "object") {
            commit("setVotes", data);
            resolve();
          } else {
            console.log(data);
          }
        });
      });
    }
  },
  getters: {
    votes: state => state.votes,
    pagination: state => state.pagination
  }
});
