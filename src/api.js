import axios from "axios";
const host = "https://rest.vmeste-region.ru/api/";
export function getVotes({ perPage, page }, cb) {
  let p = "";
  if (page) {
    p = "&page=" + page;
  }
  const url = host + "votes?expired=true&perPage=" + perPage + p;
  axios
    .get(url)
    .then(response => {
      const r = response.data;
      if (r.success) {
        const data = r.data;
        cb(data);
      } else {
        cb("Произошла ошибка при получении данных");
      }
    })
    .catch(e => {
      cb("Ошибка: " + e);
    });
}
